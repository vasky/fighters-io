import React, { ForwardedRef, Ref, RefObject } from "react";

import "./input.css";

export interface InputProps {
  placeholder?: string;
  onChange: (nextValue: string) => void;
  value: string;
  className?: string;
  ref?: Ref<HTMLInputElement>;
  onPressEnter?: () => void;
}

const Input: React.FC<InputProps> = React.forwardRef<
  HTMLInputElement,
  React.PropsWithChildren<InputProps>
>(({ placeholder, onChange, value, className = "", onPressEnter }, ref) => {
  return (
    <input
      ref={ref}
      placeholder={placeholder}
      onChange={(e) => onChange(e.target.value)}
      value={value}
      className={`input ${className}`}
      onKeyPress={(e) => e.code === "Enter" && onPressEnter?.()}
    />
  );
});

export default Input;

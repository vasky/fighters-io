import React from "react";
import { canvasStore } from "../store/canvasStore";
import { makeAutoObservable } from "mobx";
import { Sprite } from "./sprite";
import spriteHero1Run from "./../assets/images/player-sprites/martial-hero/Run.png";
import spriteHero1RunLeft from "./../assets/images/player-sprites/martial-hero/Run-left.png";
import spriteHero1Attack from "./../assets/images/player-sprites/martial-hero/Attack1.png";
import spriteHero1AttackLeft from "./../assets/images/player-sprites/martial-hero/Attack1-left.png";
import spriteHero1Stay from "./../assets/images/player-sprites/martial-hero/Idle.png";
import spriteHero1Died from "./../assets/images/player-sprites/martial-hero/Death.png";
import { gameStore } from "../store/gameStore";
import {
  ATTACK_COLLIDER_BOX_HEIGHT,
  ATTACK_COLLIDER_BOX_WIDTH,
} from "../contsants/contsants";

export class Player {
  ctx?: CanvasRenderingContext2D;
  height = 0;
  width = 0;
  x = 0;
  y = 0;
  velocityX = 0;
  velocityY = 0;
  keyListenerDown;
  keyListenerUp;
  pressedButtons = {
    a: false,
    w: false,
    s: false,
    d: false,
    space: false,
  };
  gravityPower = 1;
  playerSpeed = 10;
  health = 100;
  spriteRun?: Sprite;
  spriteAttack?: Sprite;
  spriteRunLeft?: Sprite;
  spriteAttackLeft?: Sprite;
  spriteStay?: Sprite;
  spriteDied?: Sprite;
  playerDied?: boolean = false;
  attacking?: boolean = false;

  constructor(
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    height: number,
    width: number,
    disabled?: boolean
  ) {
    this.ctx = ctx;
    if (height) this.height = height;

    if (width) this.width = width;
    if (x) this.x = x;
    if (y) this.y = y;

    makeAutoObservable(this);

    this.spriteRun = this.getSpritePlayer(this.ctx, spriteHero1Run, 7);
    this.spriteAttack = this.getSpritePlayer(
      this.ctx,
      spriteHero1Attack,
      5,
      false,
      () => {
        gameStore.socket?.emit("finishAttack");
      }
    );
    this.spriteRunLeft = this.getSpritePlayer(this.ctx, spriteHero1RunLeft, 7);
    this.spriteAttackLeft = this.getSpritePlayer(
      this.ctx,
      spriteHero1AttackLeft,
      5,
      false,
      () => {
        gameStore.socket?.emit("finishAttack");
      }
    );
    this.spriteStay = this.getSpritePlayer(this.ctx, spriteHero1Stay, 7);
    this.spriteDied = this.getSpritePlayer(this.ctx, spriteHero1Died, 5, true);

    if (!disabled) {
      this.keyListenerDown = document.addEventListener(
        "keydown",
        (e: KeyboardEvent) => this.handleKeys(e, true)
      );
      this.keyListenerUp = document.addEventListener(
        "keyup",
        (e: KeyboardEvent) => this.handleKeys(e, false)
      );
    }
  }

  update() {
    const topYStayGround = canvasStore.height - this.height;
    const ctx = canvasStore.ctx;

    if (this.pressedButtons.w && this.y === topYStayGround) {
      this.velocityY -= 27;
    }

    this.x += this.velocityX;
    this.y += this.velocityY;

    this.drawPlayer();
    if (this.attacking) {
      this.drawCollisionAttack();
    }

    if (this.health <= 0) {
      this.playerDied = true;
    }

    if (this.velocityY < 0 || this.y < topYStayGround) {
      this.velocityY += this.gravityPower;
    } else {
      this.velocityY = 0;
      this.y = topYStayGround;
    }

    if (this.pressedButtons.d) {
      this.velocityX = this.playerSpeed;
    } else if (this.pressedButtons.a) {
      this.velocityX = -this.playerSpeed;
    } else {
      this.velocityX = 0;
    }

    if (this.pressedButtons.s && this.y < topYStayGround) {
      this.velocityY += 10;
    }

    // requestAnimationFrame(() => this.update());
  }

  handleKeys = (e?: KeyboardEvent, nextVelocity?: boolean) => {
    if (!e || nextVelocity === undefined) {
      return;
    }

    gameStore.socket?.emit(nextVelocity ? "keyDown" : "keyUp", {
      data: e.code,
    });

    // switch (e.code) {
    //   case "KeyW":
    //     this.pressedButtons.w = nextVelocity;
    //     break;
    //   case "KeyS":
    //     this.pressedButtons.s = nextVelocity;
    //     break;
    //   case "KeyA":
    //     this.pressedButtons.a = nextVelocity;
    //     break;
    //   case "KeyD":
    //     this.pressedButtons.d = nextVelocity;
    //     break;
    //   case "Space":
    //     this.pressedButtons.space = nextVelocity;
    //     break;
    // }
  };

  drawPlayer() {
    const ctx = canvasStore.ctx;
    if (ctx) {
      // ctx.fillStyle = "red";
      // ctx.fillRect(this.x, this.y, this.width, this.height);

      if (this.playerDied) {
        this.spriteDied?.draw(this.x - 265, this.y - 219);
      } else if (this.pressedButtons.a && this.attacking) {
        this.spriteAttackLeft?.draw(this.x - 265, this.y - 219);
      } else if (this.attacking) {
        this.spriteAttack?.draw(this.x - 265, this.y - 219);
      } else if (this.pressedButtons.d) {
        this.spriteRun?.draw(this.x - 265, this.y - 219);
      } else if (this.pressedButtons.a) {
        this.spriteRunLeft?.draw(this.x - 265, this.y - 219);
      } else {
        this.spriteStay?.draw(this.x - 265, this.y - 219);
      }
    }
  }

  drawCollisionAttack = () => {
    const ctx = canvasStore.ctx;
    if (ctx) {
      ctx.fillStyle = `rgba(0,0,12)`;
      ctx.fillRect(
        this.pressedButtons.a
          ? this.x - ATTACK_COLLIDER_BOX_WIDTH
          : this.x + this.width,
        this.y,
        ATTACK_COLLIDER_BOX_WIDTH,
        ATTACK_COLLIDER_BOX_HEIGHT
      );
    }
  };

  decreaseHealth = () => {
    this.health -= 1;
  };

  getSpritePlayer = (
    ctx: CanvasRenderingContext2D,
    image: string,
    amountFrames: number,
    finiteAnim = false,
    onFinishAnim?: () => void
  ) =>
    new Sprite(
      ctx,
      image,
      0,
      0,
      200,
      200,
      this.x - 265,
      this.y - 215,
      600,
      600,
      amountFrames,
      finiteAnim,
      onFinishAnim
    );

  destroy() {
    document.removeEventListener("keydown", this.handleKeys);
    document.removeEventListener("keyup", this.handleKeys);
  }
}

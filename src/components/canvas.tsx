import React, { useEffect, useRef } from "react";
import { canvasStore } from "./../store/canvasStore";

const Canvas: React.FC = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  const height = 576,
    width = 1024;

  useEffect(() => {
    const ctx = canvasRef.current?.getContext("2d");
    if (ctx) {
      canvasStore.setCanvas(ctx);
    }
  });

  return (
    <div
      className="canvas-wrapper"
      style={{
        minHeight: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <canvas width={width} height={height} ref={canvasRef} />
    </div>
  );
};

export default Canvas;

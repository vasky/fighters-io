import React, {
  InputHTMLAttributes,
  RefObject,
  useEffect,
  useRef,
  useState,
} from "react";
import Button from "../button/button";
import Input from "../input/input";

import "./join-room-modal.css";

export interface JoinRoomModalProps {
  onJoinRoom?: (roomName: string) => void;
}

const JoinRoomModal: React.FC<JoinRoomModalProps> = ({ onJoinRoom }) => {
  const [roomName, setRoomName] = useState("");
  const inputRef = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, [inputRef]);
  return (
    <div className="join-room-modal">
      <Input
        ref={inputRef}
        value={roomName}
        onChange={(nextValue) => setRoomName(nextValue)}
        placeholder="Имя комнаты"
        className="join-room-modal__input"
        onPressEnter={() => onJoinRoom?.(roomName)}
      />
      <Button
        onClick={() => onJoinRoom?.(roomName)}
        className="join-room-modal__button"
      >
        Войти
      </Button>
    </div>
  );
};

export default JoinRoomModal;

import React from "react";

import "./health-bar.css";

export interface HealthBarProps {
  progress: number;
  flipped?: boolean;
}

const HealthBar: React.FC<HealthBarProps> = ({ progress, flipped }) => {
  return (
    <div className={`health-bar ${flipped ? "health-bar--flipped" : ""}`}>
      <div className="health-bar__wrapper" />
      <div className="health-bar__progress-wrapper">
        <div
          className="health-bar__progress-line"
          style={{ width: progress + "%" }}
        />
      </div>
    </div>
  );
};

export default HealthBar;

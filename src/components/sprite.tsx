export class Sprite {
  ctx?: CanvasRenderingContext2D;
  sprite?: CanvasImageSource;
  sourceX: number = 0;
  sourceY: number = 0;
  sourceWidth: number = 0;
  sourceHeight: number = 0;
  x?: number;
  y?: number;
  width?: number;
  height?: number;
  currentFrame = 0;
  amountFrames: number = 1;
  frameCounter = 0;
  finiteAnime?: boolean;
  onFinishAnim?: () => void;

  constructor(
    ctx: CanvasRenderingContext2D,
    sprite: string,
    sourceX: number,
    sourceY: number,
    sourceWidth: number,
    sourceHeight: number,
    x?: number,
    y?: number,
    width?: number,
    height?: number,
    amountFrames?: number,
    finiteAnime?: boolean,
    onFinishAnim?: () => void
  ) {
    this.ctx = ctx;

    if (amountFrames) this.amountFrames = amountFrames;

    const nextSprite = new Image();
    nextSprite.onload = () => {
      this.sprite = nextSprite;
    };
    nextSprite.src = sprite;
    this.sourceX = sourceX;
    this.sourceY = sourceY;
    this.sourceWidth = sourceWidth;
    this.sourceHeight = sourceHeight;
    this.finiteAnime = finiteAnime;
    this.onFinishAnim = onFinishAnim;

    if (
      x !== undefined &&
      y !== undefined &&
      width !== undefined &&
      height !== undefined
    ) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
    }
  }
  draw(x?: number, y?: number) {
    if (this.sprite && this.ctx) {
      if (
        this.x !== undefined &&
        this.y !== undefined &&
        this.width !== undefined &&
        this.height !== undefined
      ) {
        if (this.frameCounter >= 7) {
          this.frameCounter = 0;
        }

        if (this.frameCounter === 0) {
          this.frameCounter += 1;

          if (this.currentFrame + 1 <= this.amountFrames) {
            this.currentFrame += 1;
          } else {
            if (!this.finiteAnime) {
              this.currentFrame = 0;
            }
            this.onFinishAnim?.();
          }
        } else {
          this.frameCounter += 1;
        }

        const spriteY = y === undefined ? this.y : y;
        const spriteX = x === undefined ? this.x : x;

        this.ctx.drawImage(
          this.sprite,
          this.currentFrame * this.sourceWidth,
          this.sourceY,
          this.sourceWidth,
          this.sourceHeight,
          spriteX,
          spriteY,
          this.width,
          this.height
        );
      } else {
        this.ctx.drawImage(
          this.sprite,
          this.sourceX,
          this.sourceY,
          this.sourceWidth,
          this.sourceHeight
        );
      }
    }
  }
}

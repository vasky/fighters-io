import Canvas from "./components/canvas";
import { useEffect } from "react";
import { gameStore } from "./store/gameStore";
import HealthBar from "./components/health-bar/health-bar";
import { observer } from "mobx-react-lite";
import JoinRoomModal from "./components/join-room-modal/join-room-modal";

const App = () => {
  // useEffect(() => {
  //   gameStore.init();
  // }, []);

  const healthProgressPlayer1 =
    gameStore.player1?.health !== undefined ? gameStore.player1?.health : 100;
  const healthProgressPlayer2 =
    gameStore.player2?.health !== undefined ? gameStore.player2?.health : 100;

  return (
    <>
      {!gameStore.successSignInRoom && (
        <JoinRoomModal onJoinRoom={(roomName) => gameStore.init(roomName)} />
      )}
      <HealthBar progress={healthProgressPlayer1} />
      <HealthBar progress={healthProgressPlayer2} flipped />
      <Canvas />
    </>
  );
};

export default observer(App);

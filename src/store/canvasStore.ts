import { makeAutoObservable } from "mobx";

export const canvasStore = makeAutoObservable<{
  ctx?: CanvasRenderingContext2D;
  setCanvas: (nextCtx: CanvasRenderingContext2D) => void;
  height: number;
  width: number;
}>({
  ctx: undefined,
  height: 576,
  width: 1024,
  setCanvas: (nextCtx: CanvasRenderingContext2D) => {
    canvasStore.ctx = nextCtx;
    canvasStore.ctx.fillStyle = "pink";
    canvasStore.ctx.fillRect(0, 0, canvasStore.width, canvasStore.height);
  },
});

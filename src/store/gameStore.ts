import { makeAutoObservable } from "mobx";
import { Player } from "../components/player";
import { canvasStore } from "./canvasStore";
import { ATTACK_COLLIDER_BOX_WIDTH } from "../contsants/contsants";
import socketIOClient, { Socket } from "socket.io-client";
import { Sprite } from "../components/sprite";
import bgImage from "./../assets/images/background/bg.png";

export const gameStore = makeAutoObservable<{
  ctx?: CanvasRenderingContext2D;
  init: (roomName: string) => void;
  update: () => void;
  updatePlayer: (nextState: { players: Player[] }) => void;
  player1: Player | null;
  player2: Player | null;
  bg: Sprite | null;
  socket?: Socket;
  successSignInRoom: boolean;
}>({
  player1: null,
  player2: null,
  bg: null,
  socket: undefined,
  successSignInRoom: false,
  init: (roomName) => {
    const ctx = canvasStore.ctx;
    if (ctx) {
      gameStore.socket = socketIOClient(
        "https://socket-fighters-server.herokuapp.com"
      );
      gameStore.socket.emit("init", { roomName });
      // gameStore.socket.on("init", (msg) => console.log(msg));
      gameStore.socket.on("successSignInRoom", (msg) => {
        // console.log("successSignInRoom", msg);
        gameStore.successSignInRoom = true;
        console.log("test");
      });

      gameStore.socket.on(
        "updatePlayerState",
        (nextState: { players: Player[] }) => {
          // console.log(nextState);
          gameStore.updatePlayer(nextState);
        }
      );
      gameStore.player1 = new Player(
        ctx,
        30,
        canvasStore.height - 150,
        150,
        75
      );
      gameStore.player2 = new Player(
        ctx,
        350,
        canvasStore.height - 150,
        150,
        75,
        true
      );
      gameStore.update();

      gameStore.bg = new Sprite(
        ctx,
        bgImage,
        0,
        0,
        canvasStore.width,
        canvasStore.height
      );
      // player1.drawPlayer();
    }
  },

  updatePlayer: (nextState: { players: Player[] }) => {
    if (
      gameStore.player1 &&
      gameStore.player2 &&
      nextState.players[0] &&
      nextState.players[1]
    ) {
      gameStore.player1 = Object.assign(
        gameStore.player1,
        nextState.players[0]
      );
      gameStore.player2 = Object.assign(
        gameStore.player2,
        nextState.players[1]
      );
    }
  },

  // log

  update: () => {
    setInterval(() => {
      gameStore.bg?.draw();

      if (gameStore.player1) {
        gameStore.player1.update();
      }
      if (gameStore.player2) {
        gameStore.player2.update();
      }
    }, 1000 / 60);
    // requestAnimationFrame(() => gameStore.update());
  },
});
